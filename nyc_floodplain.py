"""
NYC Floodplain Analysis - Terence Barrett, March 2024
"""
import geopandas as gpd
import matplotlib.pyplot as plt
import pandas as pd


# Parameters
community_districts_shp = 'data/Community Districts/geo_export_ae7ecfd5-1d60-4171-ab76-4e2894b9f83d.shp'
floodplain_shp = 'data/Sea Level Rise Maps (2050s 500-year Floodplain)/geo_export_dbd152c8-b86e-421f-8f1c-f011e61276bc.shp'
cd_boroughs = {1: 'Manhattan', 2: 'Bronx', 3: 'Brooklyn', 4: 'Queens', 5: 'Staten Island'}
crs_epsg_to_project_to = 32618  # WGS84 18N, meter


def read_shp_and_project(path_to_shp, projection_epsg):
    """
    Read shapefile into geodataframe, verify CRS, project to target CRS,
    and verify projection was successful
    """
    print(f'\nProcessing shapefile: {path_to_shp}\n')
    input_gdf = gpd.read_file(path_to_shp)
    print('Input CRS:\n', repr(input_gdf.crs))
    projected_gdf = input_gdf.to_crs(epsg=projection_epsg)
    print('Projected CRS:\n', repr(projected_gdf.crs))
    return projected_gdf


print('Create community-district geodataframe, adding a field '
      'with borough names')
community_districts_projected_gdf = (
    read_shp_and_project(path_to_shp=community_districts_shp,
                         projection_epsg=crs_epsg_to_project_to)
)
community_districts_projected_gdf.drop(columns=['shape_area', 'shape_leng'],
                                       inplace=True)
community_districts_projected_gdf['boro_cd'] = \
    community_districts_projected_gdf['boro_cd'].astype(int)
community_districts_projected_gdf['boro_code'] = \
    community_districts_projected_gdf[
        'boro_cd'
    ].astype('str').str.slice(stop=1).astype(int)
community_districts_projected_gdf['boro_name'] = \
    community_districts_projected_gdf['boro_code'].map(cd_boroughs)
community_districts_projected_gdf.set_index('boro_cd', drop=False, inplace=True)

print('Create floodplain-by-district geodataframe and calculate projected '
      'floodplain area by district')
floodplain_projected_gdf = (
    read_shp_and_project(path_to_shp=floodplain_shp,
                         projection_epsg=crs_epsg_to_project_to)
)
floodplain_projected_gdf.drop(columns=['abfe_0_2pc', 'fld_zone', 'gridcode',
                                       'join_count', 'shape_area', 'shape_leng',
                                       'static_bfe'],
                              inplace=True)
floodplain_by_district_gdf = \
    gpd.overlay(
        community_districts_projected_gdf[['boro_cd', 'geometry']],
        floodplain_projected_gdf,
        how='intersection'
    ).dissolve(
        by='boro_cd'
    )
floodplain_by_district_gdf['floodplain_area_sq_km'] = \
        1e-6 * floodplain_by_district_gdf.geometry.area

print('Join floodplain areas to districts (filling in zero for districts with '
      'no floodplain); calculate projected district areas and fraction of that '
      'that is floodplain')
community_districts_projected_gdf.drop(columns='boro_cd', inplace=True)
community_districts_with_floodplain_gdf = \
    gpd.GeoDataFrame(community_districts_projected_gdf.merge(
        right=floodplain_by_district_gdf,
        how='left',
        left_index=True,
        right_index=True,
        suffixes=('_district', '_floodplain')
    ).sort_index().fillna(value={'floodplain_area_sq_km': 0}),
                          geometry='geometry_district',
                          crs=f'EPSG:{crs_epsg_to_project_to}')
community_districts_with_floodplain_gdf['total_area_sq_km'] = \
    1e-6 * community_districts_with_floodplain_gdf.geometry.area
community_districts_with_floodplain_gdf['floodplain_fraction'] = \
    community_districts_with_floodplain_gdf['floodplain_area_sq_km'] \
    / community_districts_with_floodplain_gdf['total_area_sq_km']

print('Create and write district report')
community_districts_floodplain_report_df = \
    community_districts_with_floodplain_gdf.drop(
        columns=['geometry_district', 'geometry_floodplain', 'boro_code'])
community_districts_floodplain_report_df.to_csv('results/district_floodplain.csv')

print('Create and write borough/city summary report')
boroughs_floodplain_report_df = \
    community_districts_floodplain_report_df.drop(
        columns=['floodplain_fraction']
    ).groupby(by='boro_name').sum()
city_stats_df = pd.DataFrame.from_dict(
    {'Total': [
        boroughs_floodplain_report_df['floodplain_area_sq_km'].sum(),
        boroughs_floodplain_report_df['total_area_sq_km'].sum()
    ]},
    orient='index',
    columns=['floodplain_area_sq_km', 'total_area_sq_km']
)
boroughs_floodplain_report_df = \
    pd.concat([boroughs_floodplain_report_df, city_stats_df])
boroughs_floodplain_report_df['floodplain_fraction'] = \
    boroughs_floodplain_report_df['floodplain_area_sq_km'] \
    / boroughs_floodplain_report_df['total_area_sq_km']
boroughs_floodplain_report_df.to_csv('results/borough_floodplain.csv')

print('Create and write map image')
fig = plt.figure(figsize=(10, 10), dpi=400)
ax = plt.gca()
cd_plot = community_districts_projected_gdf.plot(
    ax=ax, facecolor="gray", edgecolor="black",
    linewidth=0.5, column='boro_code')
water_plot = floodplain_projected_gdf.plot(
    ax=ax, facecolor="blue", edgecolor="none",
    alpha=0.25)
floodplain_plot = floodplain_by_district_gdf.plot(
    ax=ax, facecolor="blue", edgecolor="blue",
    linewidth=0.5, alpha=0.5)
plt.savefig('results/nyc_floodplain_map.png')
