# Geopandas Example

## VACC set up -- first time only
- Install this code project into a directory in your VACC home space 
  - `$ cd ~`
  - `$ mkdir code-examples`
  - `$ cd code-examples`
  - Then use one of these methods:
    - Clone method
      - `$ git clone https://gitlab.uvm.edu/Terence.Barrett/geopandas-example.git`
    - Copy method
      - Download a `tar` file of the code project from 
      https://gitlab.uvm.edu/Terence.Barrett/geopandas-example to your local 
      machine
      - Copy that file to the cluster via Filezilla, `scp`, etc
        - For example, from your local machine: 
          - `> scp geopandas-example-main.tar tcbarret@vacc-user1.uvm.edu:~/code-examples/geopandas-example-main.tar`
      - On the cluster, extract the tar file
        - `$ cd ~/code-examples`
        - `$ tar -xvf geopandas-example-main.tar`
        - Rename with `$ mv geopandas-example-main geopandas-example`
- Create the project's conda environment
  - If you have your own conda installed (Anaconda or Miniconda), enter 
  `$ conda deactivate` repeatedly until no environments are active, 
  including the "base" environment
  - `$ module purge` 
  - `$ module load python3.10-anaconda/2023.03-1` 
    - This is so that the conda environment we build is compatible with OOD 
    Notebook running
  - `$ source ${ANACONDA_ROOT}/etc/profile.d/conda.sh`
    - This will ensure that your use of conda in this session is the conda 
    in the module loaded above
      - Confirm this with `$ which conda`, which should return 
      `/gpfs1/sw/x86_64/rh7/pkgs/python3.10-anaconda/2023.03-1/bin/conda`
  - `$ cd ~/code-examples/geopandas-example`
  - `$ conda env create -f environment_VACC.yml`
    - The env will be created in `~/.conda/envs/geopandas-example-env`
  - `$ conda activate geopandas-example-env`
  - `$ python -m ipykernel install --user --name=geopandas-example-env`
    - A notebook kernel, for use by OOD, will be installed in 
    `~/.local/share/jupyter/kernels/geopandas-example-env`
- Create directory to run batch jobs from (and so OUT files will be saved here)
  - `$ mkdir ~/batch_working`
- If you have your own conda installation you'd like to reset to be your 
default again now that this setup is complete, and you will not be immediately 
trying out Method 1:
  - Enter `$ conda deactivate` repeatedly until no environments are active
  - `$ module purge`
  - `$ <path-to-your-miniconda-or-anaconda-root>/bin/conda init`
    - Example: `$ /gpfs1/home/t/c/tcbarret/miniconda3/bin/conda init`
  - `$ source ~/.bashrc`

## VACC Running, method 1: Run on login node, for testing setup
- Note: Run the code this way only to test the setup of this 
non-resource-intensive process; see the guidance on running processes
on the login node
- When starting a session after the initial setup, above, was completed in a 
prior session, perform these steps; otherwise if continuing directly from 
the setup above, skip these steps:
  - If you have your own conda installed (Anaconda or Miniconda), enter 
  `$ conda deactivate` repeatedly until no environments are active, 
  including the "base" environment  
  - `$ module purge` 
  - `$ module load python3.10-anaconda/2023.03-1` 
  - `$ source ${ANACONDA_ROOT}/etc/profile.d/conda.sh`
  - `$ conda activate geopandas-example-env`
- `$ cd ~/code-examples/geopandas-example`
- `$ python nyc_floodplain.py`
- See the output in the `~/code-examples/geopandas-example/results` and 
compare to that in the `~/code-examples/geopandas-example/example_results` 
folder to see if it processed correctly
- If you have your own conda installation you'd like to reset to be your 
default again now that this test is complete:
  - Enter `$ conda deactivate` repeatedly until no environments are active
  - `$ module purge`
  - `$ <path-to-your-miniconda-or-anaconda-root>/bin/conda init`
    - Example: `$ /gpfs1/home/t/c/tcbarret/miniconda3/bin/conda init`
  - `$ source ~/.bashrc`
  
## VACC Running, method 2: Run as a batch job, the standard way to run processes on the cluster
- `$ cd ~/batch_working`
- `$ sbatch ../code-examples/geopandas-example/slurm_nyc_floodplain.sh`
- Monitor job progress with `$ squeue --me`
- Monitor the logging from the job to the OUT file in the `~/batch_working` 
directory with `$ tail -f <logfile>.out` or view the full log after the run 
completes with `$ more <logfile>.out` or `$ cat <logfile>.out` 
(replacing <logfile> with the actual file name)
- See the output in the `~/code-examples/geopandas-example/results` folder and 
compare to that in the `~/code-examples/geopandas-example/example_results` 
folder to see if it processed correctly

## VACC Running, method 3: Run in a Jupyter Notebook via Open OnDemand
- Navigate to https://vacc-ondemand.uvm.edu/
  - Documentation on OOD is available here: 
  https://www.uvm.edu/vacc/kb/knowledge-base/ondemand/
- Select "JupyterLab / Jupyter Notebook"
- Enter in the form:
  - Your slurm account (see instructions on the form)
  - Mode: Jupyter Notebook
  - Partition: Bluemoon
  - Python version: 3.10
  - Number of hours: 1
  - Number of nodes: 1
  - Number of cores: 1
- When the session has started, click "Connect to Jupyter"
- Navigate to the notebook, `nyc_floodplain.ipynb`, and click to open it in 
a new tab
- From the top menu of the notebook, select Kernel > Change 
Kernel > geopandas-example-env
- Preview what the cell outputs should look like, and then from the top menu 
select Cell > All Output > Clear to remove all the example output
- Run all cells of the notebook with Cell > Run All, and output should be
generated and displayed that matches the example output seen originally
- When done using the Notebook, close its tab, return to the OOD tab where 
the session info is shown, and click "Delete" to end the session and free up the 
cluster resources for others to use.
