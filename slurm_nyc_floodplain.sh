#!/bin/bash

### Slurm header section ###

#SBATCH --job-name=nycflood
#SBATCH --partition=short
#SBATCH --time=00:10:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=1GB
#SBATCH --mail-type=ALL

### Executable section ###

# Echo each command to the log file
set -x

# Link log file to one with a preferred name, and in the output folder
ln -f "slurm-${SLURM_JOB_ID}.out" "${SLURM_JOB_NAME}_${SLURM_JOB_ID}.out"

# Load conda and activate environment
module purge
module load python3.10-anaconda/2023.03-1
source "${ANACONDA_ROOT}/etc/profile.d/conda.sh"

# Change to working directory
cd "${HOME}/code-examples/geopandas-example"

# Run LiDAR Dataprep
conda run -n geopandas-example-env python nyc_floodplain.py
